# co2.click Maker-Edition CO2 sensor enclosures

## Description

This repository hosts enclosure designs for the co2.click Maker-Edition CO2 sensor.

Simply print on your 3D printer and have fun.

## Contribution

- Create an issue
- Create a merge request for the issue
- Code, test, repeat
- When ready submit your merge request for approval

Pro tip: Use `shipit-cli` (https://gitlab.com/intello/shipit-cli)


